//----------------------------------------------------------------------------------------------------------------------
// Planet
//----------------------------------------------------------------------------------------------------------------------

import { Entity, EntityStatus, Location, PlanetEntity, SquadronEntity } from '../interfaces/entities';

//----------------------------------------------------------------------------------------------------------------------

export class Planet implements PlanetEntity
{
    readonly name : string;
    readonly type = 'planet';
    readonly location : Location;

    // Planets can have squadrons deployed to them
    readonly squadrons : Set<SquadronEntity> = new Set();

    public status : EntityStatus = EntityStatus.NOMINAL;

    // -----------------------------------------------------------------------------------------------------------------

    constructor(name : string, location : Location)
    {
        this.name = name;
        this.location = location;
    } // end constructor

    public deploySquadron(squadron : SquadronEntity) : void
    {
        this.squadrons.add(squadron);
    }

    public removeSquadron(squadron : SquadronEntity) : void
    {
        this.squadrons.delete(squadron);
    }

    public async attack(_target : Entity) : Promise<boolean>
    {
        // Planets can't attack
        return false;
    }

    public async defend(entity : Entity) : Promise<boolean>
    {
        // Planets defend with their squadrons
        for(const squad of this.squadrons)
        {
            // Attempt to attack the entity
            // eslint-disable-next-line no-await-in-loop
            if(await squad.attack(entity))
            {
                return true;
            }
        }

        // If we've gotten here, we've failed
        return false;
    }
} // end Planet

//----------------------------------------------------------------------------------------------------------------------

