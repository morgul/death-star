// ---------------------------------------------------------------------------------------------------------------------
// Death Star Entity
// ---------------------------------------------------------------------------------------------------------------------

import { HyperDriveStatus, ShieldStatus, SuperLaserStatus } from '../interfaces/technology';
import { Entity, EntityStatus, Location, SquadronEntity, StationEntity } from '../interfaces/entities';

// ---------------------------------------------------------------------------------------------------------------------

export class DeathStarBattleStation implements StationEntity
{
    #status : EntityStatus = EntityStatus.NOMINAL;

    readonly name : string;
    readonly type = 'station';
    readonly thermalExhaustPortSize = 2;

    public location : Location;
    public hyperDriveStatus : HyperDriveStatus = HyperDriveStatus.INACTIVE;
    public superLaserStatus : SuperLaserStatus = SuperLaserStatus.INACTIVE;

    // TODO: Figure out how to generate a shield large enough to activate this!
    public shieldStatus : ShieldStatus = ShieldStatus.INACTIVE;

    // -----------------------------------------------------------------------------------------------------------------

    get status() : EntityStatus { return this.#status; }
    set status(val : EntityStatus)
    {
        if(val === EntityStatus.DESTROYED)
        {
            this.hyperDriveStatus = HyperDriveStatus.DESTROYED;
            this.superLaserStatus = SuperLaserStatus.DESTROYED;
            this.shieldStatus = ShieldStatus.DESTROYED;
        }

        this.#status = val;
    }

    // -----------------------------------------------------------------------------------------------------------------

    constructor(name : string, location : Location)
    {
        this.name = name;
        this.location = location;
    }

    // -----------------------------------------------------------------------------------------------------------------

    public async attack(target : Entity) : Promise<boolean>
    {
        if(target.type === 'squadron')
        {
            return true;
        }
        else if(target.type === 'planet' || target.type === 'fleet')
        {
            // Planets get a chance to defend
            if(await target.defend(this))
            {
                return false;
            }

            // Intentionally fall through to super laser code.
        }

        // For everything else, there's a super laser
        if(this.superLaserStatus === SuperLaserStatus.INACTIVE)
        {
            this.superLaserStatus = SuperLaserStatus.ACTIVE;

            return new Promise((resolve) =>
            {
                // Simulate Travel time
                setTimeout(() =>
                {
                    this.superLaserStatus = SuperLaserStatus.INACTIVE;
                    resolve(true);
                }, 3000);
            });
        }

        return false;
    }

    public async defend(attacker : Entity) : Promise<boolean>
    {
        // Rare case where attempting to defend can blow us up.
        if(attacker.type === 'squadron')
        {
            const squad = attacker as SquadronEntity;
            const defend = this.shieldStatus === ShieldStatus.ACTIVE
                || squad.minTargetSize > this.thermalExhaustPortSize;

            if(!defend)
            {
                this.status = EntityStatus.DESTROYED;
            }
        }

        // But we're mainly untouchable
        return true;
    }

    public async jumpToHyperspace(location : Location) : Promise<boolean>
    {
        if(this.hyperDriveStatus === HyperDriveStatus.INACTIVE)
        {
            this.hyperDriveStatus = HyperDriveStatus.ACTIVE;

            return new Promise((resolve) =>
            {
                // Simulate Travel time
                setTimeout(() =>
                {
                    this.hyperDriveStatus = HyperDriveStatus.INACTIVE;
                    this.location = location;
                    resolve(true);
                }, 3000);
            });
        }

        return false;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
