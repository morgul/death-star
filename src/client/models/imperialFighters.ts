// ---------------------------------------------------------------------------------------------------------------------
// Imperial Fighter Squadrons
// ---------------------------------------------------------------------------------------------------------------------

import { FighterSquadron } from './fighterSquadron';

// ---------------------------------------------------------------------------------------------------------------------

export class TIESquadron extends FighterSquadron
{
    constructor(name : string)
    {
        super(name, 5);
    }
}

export class TIEInterceptorSquadron extends FighterSquadron
{
    constructor(name : string)
    {
        super(name, 2);
    }
}

export class TIEBomberSquadron extends FighterSquadron
{
    constructor(name : string)
    {
        super(name, 7);
    }
}

// ---------------------------------------------------------------------------------------------------------------------
