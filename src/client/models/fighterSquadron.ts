// ---------------------------------------------------------------------------------------------------------------------
// Fighter Squadron
// ---------------------------------------------------------------------------------------------------------------------

import { Entity, EntityStatus, Location, SquadronEntity } from '../interfaces/entities';

// ---------------------------------------------------------------------------------------------------------------------

export class FighterSquadron implements SquadronEntity
{
    readonly name : string;
    readonly type = 'squadron';
    readonly minTargetSize : number;

    public location ?: Location;
    public status : EntityStatus = EntityStatus.NOMINAL;

    // -----------------------------------------------------------------------------------------------------------------

    constructor(name : string, minTargetSize = 5, location ?: Location)
    {
        this.name = name;
        this.location = location;
        this.minTargetSize = minTargetSize;
    }

    // -----------------------------------------------------------------------------------------------------------------

    public async attack(target : Entity) : Promise<boolean>
    {
        if(target.type === 'squadron')
        {
            // 50-50 chance of success
            return Math.random() < 0.5;
        }
        else
        {
            return !(await target.defend(this));
        }
    }

    public async defend(_attacker : Entity) : Promise<boolean>
    {
        return false;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
