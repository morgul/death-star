//----------------------------------------------------------------------------------------------------------------------
// Fleet
//----------------------------------------------------------------------------------------------------------------------

import { Entity, EntityStatus, FleetEntity, Location, SquadronEntity } from '../interfaces/entities';

//----------------------------------------------------------------------------------------------------------------------

export class Fleet implements FleetEntity
{
    readonly name : string;
    readonly type = 'fleet';
    readonly location : Location;

    // Fleets can have squadrons deployed to them
    readonly squadrons : Set<SquadronEntity> = new Set();

    public status : EntityStatus = EntityStatus.NOMINAL;

    // -----------------------------------------------------------------------------------------------------------------

    constructor(name : string, location : Location)
    {
        this.name = name;
        this.location = location;
    } // end constructor

    public deploySquadron(squadron : SquadronEntity) : void
    {
        this.squadrons.add(squadron);
    }

    public removeSquadron(squadron : SquadronEntity) : void
    {
        this.squadrons.delete(squadron);
    }

    public async attack(target : Entity) : Promise<boolean>
    {
        if(target.type === 'fleet')
        {
            // 50-50 chance of success
            return Math.random() < 0.5;
        }
        else
        {
            return !(await target.defend(this));
        }
    }

    public async defend(entity : Entity) : Promise<boolean>
    {
        // Defending Fleets get to attack first
        if(await this.attack(entity))
        {
            entity.status = EntityStatus.DESTROYED;
            return true;
        }

        // Fleets defend with their squadrons
        for(const squad of this.squadrons)
        {
            // Attempt to attack the entity
            // eslint-disable-next-line no-await-in-loop
            if(await squad.attack(entity))
            {
                return true;
            }
        }

        // If we've gotten here, we've failed
        return false;
    }
} // end Fleet

//----------------------------------------------------------------------------------------------------------------------

