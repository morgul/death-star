//----------------------------------------------------------------------------------------------------------------------
// Death Star Client Application
//----------------------------------------------------------------------------------------------------------------------

import Vue from 'vue';
import VueRouter from 'vue-router';

// VueRX
import VueRx from 'vue-rx';

// Bootstrap Vue
import BootstrapVue from 'bootstrap-vue';

// Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { far } from '@fortawesome/pro-regular-svg-icons';
import { fas } from '@fortawesome/pro-solid-svg-icons';
import { fal } from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon, FontAwesomeLayers } from '@fortawesome/vue-fontawesome';

// Managers
import allianceMan from './managers/alliance';
import empireMan from './managers/empire';
import galaxyMan from './managers/galaxy';

// Views
import App from './app.vue';

// Pages
import HomePage from './pages/home.vue';
import ControlPanelPage from './pages/controlPanel.vue';

// Site Style
import './scss/theme.scss';

//----------------------------------------------------------------------------------------------------------------------
// Font Awesome
// ---------------------------------------------------------------------------------------------------------------------

library.add(fab, far, fas, fal);
Vue.component('Fa', FontAwesomeIcon);
Vue.component('FaLayers', FontAwesomeLayers);

// ---------------------------------------------------------------------------------------------------------------------
// VueRX
// ---------------------------------------------------------------------------------------------------------------------

Vue.use(VueRx);

// ---------------------------------------------------------------------------------------------------------------------
// Bootstrap Vue
// ---------------------------------------------------------------------------------------------------------------------

import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(BootstrapVue);

//----------------------------------------------------------------------------------------------------------------------
// Vue Router
//----------------------------------------------------------------------------------------------------------------------

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', name: 'home', component: HomePage },
        { path: '/control-panel', name: 'control-panel', component: ControlPanelPage }
    ]
});

//----------------------------------------------------------------------------------------------------------------------
// Setup Vue App
//----------------------------------------------------------------------------------------------------------------------

async function initialize() : Promise<void>
{
    await galaxyMan.init();
    await allianceMan.init();
    await empireMan.init();

    new Vue({
        components: { App },
        router,
        el: '#app',
        template: '<App/>'
    });
}

// Start the application
initialize();

//----------------------------------------------------------------------------------------------------------------------
