// ---------------------------------------------------------------------------------------------------------------------
// Galaxy Manager
// ---------------------------------------------------------------------------------------------------------------------

import { BehaviorSubject, Observable } from 'rxjs';

// Interfaces
import { Entity, EntityStatus, EntityTypes, Location } from '../interfaces/entities';

// Models
import { StarSystem } from '../models/starSystem';
import { Planet } from '../models/planet';

// Resource Access
import galaxyRA from '../resource-access/galaxy';

// ---------------------------------------------------------------------------------------------------------------------

class GalaxyManager
{
    #locationsSubject : BehaviorSubject<Location[]> = new BehaviorSubject([] as Location[]);

    // -----------------------------------------------------------------------------------------------------------------

    get locations$() : Observable<Location[]> { return this.#locationsSubject.asObservable(); }

    // -----------------------------------------------------------------------------------------------------------------

    public async init() : Promise<void>
    {
        // Systems
        const AlderaanSystem = new StarSystem('Alderaan');
        const CoruscantSystem = new StarSystem('Coruscant');
        const GeonosisSystem = new StarSystem('Geonosis');
        const HothSystem = new StarSystem('Hoth');
        const YavinSystem = new StarSystem('Yavin');

        // Planets
        const Alderaan = new Planet('Alderaan', AlderaanSystem);
        const Coruscant = new Planet('Coruscant', CoruscantSystem);
        const Hoth = new Planet('Hoth', HothSystem);
        const Yavin4 = new Planet('Yavin 4', YavinSystem);

        // Add locations
        await this.addLocation(AlderaanSystem);
        await this.addLocation(CoruscantSystem);
        await this.addLocation(GeonosisSystem);
        await this.addLocation(HothSystem);
        await this.addLocation(YavinSystem);

        // Add Planets
        await galaxyRA.addEntity(Alderaan);
        await galaxyRA.addEntity(Coruscant);
        await galaxyRA.addEntity(Hoth);
        await galaxyRA.addEntity(Yavin4);
    }

    public async listEntities(location ?: Location, typeFilter ?: EntityTypes[]) : Promise<Entity[]>
    {
        return galaxyRA.listEntities(location, typeFilter);
    }

    public async getEntity<T extends Entity>(name : string) : Promise<T | undefined>
    {
        return galaxyRA.getEntity<T>(name);
    }

    public async addLocation(location : Location) : Promise<void>
    {
        await galaxyRA.addLocation(location);
        this.#locationsSubject.next(await this.listLocations());
    }

    public async listLocations() : Promise<Location[]>
    {
        return galaxyRA.listLocations();
    }

    public async getLocation<T extends Location>(name : string) : Promise<T | undefined>
    {
        return galaxyRA.getLocation<T>(name);
    }

    public async attack(attacker : Entity, defender : Entity) : Promise<boolean>
    {
        const results = await attacker.attack(defender);

        if(results)
        {
            defender.status = EntityStatus.DESTROYED;
        }

        return results;
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new GalaxyManager();

// ---------------------------------------------------------------------------------------------------------------------
