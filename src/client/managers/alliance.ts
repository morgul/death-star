//----------------------------------------------------------------------------------------------------------------------
// RebelAllianceManager
//----------------------------------------------------------------------------------------------------------------------

// Models
import { XWingSquadron, YWingSquadron } from '../models/rebelFighters';
import { Planet } from '../models/planet';

// Resource Access
import galaxyRA from '../resource-access/galaxy';

//----------------------------------------------------------------------------------------------------------------------

class RebelAllianceManager
{
    public async init() : Promise<void>
    {
        // Squadrons
        const GoldSquadron = new YWingSquadron('Gold Squadron');
        const RedSquadron = new XWingSquadron('Red Squadron');

        const Yavin4 = await galaxyRA.getEntity<Planet>('Yavin 4');
        if(Yavin4)
        {
            // Deploy our squadrons to the hidden Rebel Base
            Yavin4.deploySquadron(GoldSquadron);
            Yavin4.deploySquadron(RedSquadron);
        }
    }
} // end RebelAllianceManager

//----------------------------------------------------------------------------------------------------------------------

export default new RebelAllianceManager();

//----------------------------------------------------------------------------------------------------------------------
