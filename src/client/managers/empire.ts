//----------------------------------------------------------------------------------------------------------------------
// GalacticEmpireManager
//----------------------------------------------------------------------------------------------------------------------

import { BehaviorSubject, Observable } from 'rxjs';

// Models
import { Planet } from '../models/planet';
import { TIESquadron, TIEInterceptorSquadron, TIEBomberSquadron } from '../models/imperialFighters';
import { DeathStarBattleStation } from '../models/deathStar';

// Managers
import galaxyMan from './galaxy';

// Resource Access
import galaxyRA from '../resource-access/galaxy';

// Interfaces
import { Entity, Location } from '../interfaces/entities';
import { Fleet } from '../models/fleet';

//----------------------------------------------------------------------------------------------------------------------

class GalacticEmpireManager
{
    #deathStarSubject : BehaviorSubject<DeathStarBattleStation> = new BehaviorSubject(new DeathStarBattleStation('DS1', { name: 'Geonosis' }));
    #targetsSubject : BehaviorSubject<Entity[]> = new BehaviorSubject([] as Entity[])

    // -----------------------------------------------------------------------------------------------------------------

    get deathstar$() : Observable<DeathStarBattleStation> { return this.#deathStarSubject.asObservable(); }
    get targets$() : Observable<Entity[]> { return this.#targetsSubject.asObservable(); }

    get deathstar() : DeathStarBattleStation { return this.#deathStarSubject.getValue(); }

    // -----------------------------------------------------------------------------------------------------------------

    public async init() : Promise<void>
    {
        // We must inform the galaxy of our Technological Terror!
        galaxyRA.addEntity(this.deathstar);

        // Squadrons
        const BlackSquadron = new TIESquadron('Black Squadron');
        const HelixSquadron = new TIEBomberSquadron('Helix Squadron');
        const TitanSquadron = new TIEInterceptorSquadron('Titan Squadron');
        const AggressorSquadron = new TIESquadron('Aggressor Squadron');
        const RavagerSquadron = new TIEBomberSquadron('Ravager Squadron');

        // Planets
        const Coruscant = await galaxyRA.getEntity<Planet>('Coruscant');
        if(Coruscant)
        {
            // Deploy our squadrons to the home world
            Coruscant.deploySquadron(BlackSquadron);
            Coruscant.deploySquadron(HelixSquadron);
            Coruscant.deploySquadron(TitanSquadron);

            // Fleets
            const CoruscantDefenseFleet = new Fleet('Coruscant Defense Fleet', Coruscant.location);
            CoruscantDefenseFleet.deploySquadron(AggressorSquadron);
            CoruscantDefenseFleet.deploySquadron(RavagerSquadron);

            galaxyRA.addEntity(CoruscantDefenseFleet);
        }
    }

    public async refreshTargets() : Promise<void>
    {
        const targets = (await galaxyMan.listEntities(this.deathstar.location))
            .filter((entity) => entity !== this.deathstar);
        this.#targetsSubject.next(targets);
    }

    public async jumpToHyperspace(location : Location) : Promise<boolean>
    {
        const results = await this.deathstar.jumpToHyperspace(location);
        await this.refreshTargets();

        return results;
    }
} // end GalacticEmpireManager

//----------------------------------------------------------------------------------------------------------------------

export default new GalacticEmpireManager();

//----------------------------------------------------------------------------------------------------------------------
