// ---------------------------------------------------------------------------------------------------------------------
// Galaxy Resource Access
// ---------------------------------------------------------------------------------------------------------------------

import { Entity, EntityTypes, FleetEntity, Location, PlanetEntity } from '../interfaces/entities';

// ---------------------------------------------------------------------------------------------------------------------

class GalaxyResourceAccess
{
    readonly #entities : Set<Entity> = new Set();
    readonly #locations : Set<Location> = new Set();

    // -----------------------------------------------------------------------------------------------------------------

    public async listEntities(location ?: Location, typeFilter ?: EntityTypes[]) : Promise<Entity[]>
    {
        let entities = Array.from(this.#entities.values());

        if(location)
        {
            entities = entities.filter((entity) => entity.location?.name === location.name);
        }

        if(typeFilter && typeFilter.length > 0)
        {
            entities = entities.filter((entity) => typeFilter.includes(entity.type));
        }

        return entities;
    }

    public async getEntity<T extends Entity>(name : string) : Promise<T | undefined>
    {
        for(const entity of this.#entities)
        {
            if(entity.name === name)
            {
                return entity as T;
            }
        }
    }

    public async addEntity(entity : Entity) : Promise<void>
    {
        this.#entities.add(entity);
    }

    public async removeEntity(entity : Entity) : Promise<void>
    {
        if(entity.type === 'planet')
        {
            // Delete the planet's squadrons; we assume they've been destroyed, too.
            (entity as PlanetEntity).squadrons.forEach((squad) => this.#entities.delete(squad));
        }

        if(entity.type === 'fleet')
        {
            // Delete the fleet's squadrons; we assume they've been destroyed, too.
            (entity as FleetEntity).squadrons.forEach((squad) => this.#entities.delete(squad));
        }

        this.#entities.delete(entity);
    }

    public async listLocations() : Promise<Location[]>
    {
        return Array.from(this.#locations.values());
    }

    public async getLocation<T extends Location>(name : string) : Promise<T | undefined>
    {
        for(const location of this.#locations)
        {
            if(location.name === name)
            {
                return location as T;
            }
        }
    }

    public async addLocation(location : Location) : Promise<void>
    {
        this.#locations.add(location);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new GalaxyResourceAccess();

// ---------------------------------------------------------------------------------------------------------------------
