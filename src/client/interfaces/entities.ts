// ---------------------------------------------------------------------------------------------------------------------
// Entity Interfaces
// ---------------------------------------------------------------------------------------------------------------------

import { ShieldStatus } from './technology';

// ---------------------------------------------------------------------------------------------------------------------

export type EntityTypes = 'planet' | 'fleet' | 'station' | 'squadron';

export interface Location {
    name : string;
}

export enum EntityStatus {
    NOMINAL,
    DESTROYED
}

export interface Entity {
    location ?: Location;
    name : string;
    type : EntityTypes;
    status : EntityStatus;
    attack : (entity : Entity) => Promise<boolean>
    defend : (entity : Entity) => Promise<boolean>
}

export interface SquadronEntity extends Entity {
    location ?: Location;
    name : string;
    type : 'squadron';
    minTargetSize : number;
}

export interface StationEntity extends Entity {
    location : Location;
    name : string;
    type : 'station';
    thermalExhaustPortSize ?: number;
    shieldStatus : ShieldStatus;
}

export interface FleetEntity extends Entity {
    location : Location;
    name : string;
    type : 'fleet';
    squadrons : Set<SquadronEntity>;
}

export interface PlanetEntity extends Entity {
    location : Location;
    name : string;
    type : 'planet';
    squadrons : Set<SquadronEntity>;
}

// ---------------------------------------------------------------------------------------------------------------------
