// ---------------------------------------------------------------------------------------------------------------------
// Technology Interfaces
// ---------------------------------------------------------------------------------------------------------------------

export enum HyperDriveStatus {
    ACTIVE,
    INACTIVE,
    DISABLED,
    DESTROYED
}

export enum SuperLaserStatus {
    ACTIVE,
    INACTIVE,
    DISABLED,
    DESTROYED
}

export enum ShieldStatus {
    ACTIVE,
    INACTIVE,
    DISABLED,
    DESTROYED
}

// ---------------------------------------------------------------------------------------------------------------------
