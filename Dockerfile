#-----------------------------------------------------------------------------------------------------------------------
# Package Stage - Handle the package.json version cache invalidation problem.
#-----------------------------------------------------------------------------------------------------------------------

FROM node:14-alpine as package-tmp

RUN mkdir -p /app

COPY package.json /tmp
COPY package-lock.json /tmp

RUN awk '/version/ && !done {gsub(/[0-9]+\.[0-9]+\.[0-9]+(-rc\.[0-9]+)?/, "0.0.0"); done=1};{print}' /tmp/package.json > /app/package.json
RUN awk '/version/ && !done {gsub(/[0-9]+\.[0-9]+\.[0-9]+(-rc\.[0-9]+)?/, "0.0.0"); done=1};{print}' /tmp/package-lock.json > /app/package-lock.json

#-----------------------------------------------------------------------------------------------------------------------
# Dev Dependencies Stage - Pull down all dependencies (needed to build)
#-----------------------------------------------------------------------------------------------------------------------

FROM node:14-alpine as dev-dependencies

RUN mkdir -p /app
WORKDIR /app

COPY .npmrc /app/
COPY --from=package-tmp /app/package.json /app/package.json
COPY --from=package-tmp /app/package-lock.json /app/package-lock.json

# This is if you need to build from src.
#RUN apk add --no-cache build-base g++ python pkgconfig

RUN npm ci --no-fund

#-----------------------------------------------------------------------------------------------------------------------
# Prod Dependencies Stage - Pull down only production dependencies
#-----------------------------------------------------------------------------------------------------------------------

FROM node:14-alpine as prod-dependencies

RUN mkdir -p /app
WORKDIR /app

COPY .npmrc /app/
COPY --from=package-tmp /app/package.json /app/package.json
COPY --from=package-tmp /app/package-lock.json /app/package-lock.json

# This is if you need to build from src.
#RUN apk add --no-cache build-base g++ python pkgconfig

RUN npm ci --no-fund --production

#-----------------------------------------------------------------------------------------------------------------------
# Build Stage - Lint & Compile
#-----------------------------------------------------------------------------------------------------------------------

FROM node:14-alpine as builder

COPY --from=dev-dependencies /app /app
COPY src /app/src
COPY webpack*.js /app/
COPY ts*.json /app/
COPY package.json /app/

WORKDIR /app

RUN npm run build

#-----------------------------------------------------------------------------------------------------------------------
# Final Stage - Build the final docker image
#-----------------------------------------------------------------------------------------------------------------------

FROM node:14-alpine

MAINTAINER Ugnaught Workers <ugnaughts@workers-guild.org>

# These are build arguments we take in, so we can freeze these values inside the docker _image_.
ARG commit_sha=unknown

# These are labels for the image, just a renaming of the build variables. This is book keeping
LABEL git-commit="$commit_sha"

# We make these available so the _running code in the container_ has access to our build variables.
ENV CI_COMMIT_REF_NAME="$commit_ref"
ENV NODE_ENV="production"

# Only copy the files we actually need
COPY --from=prod-dependencies /app/node_modules /app/node_modules
COPY --from=builder /app/dist /app/dist
COPY package.json /app/

WORKDIR /app

#-----------------------------------------------------------------------------------------------------------------------

# We with a comment at the end to make it easier to figure out which process this is.
CMD [ "node", "./dist/server.js", "# death star" ]

#-----------------------------------------------------------------------------------------------------------------------
