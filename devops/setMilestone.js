#!/usr/bin/env node

// ---------------------------------------------------------------------------------------------------------------------
// Set the current Milestone
//
// This generates a bash script that is to be evaluated, as that's the only was we can set the environment variables
// for all other commands to be used.
// ---------------------------------------------------------------------------------------------------------------------

import axios from 'axios';
import semver from 'semver';

// ---------------------------------------------------------------------------------------------------------------------

const {
    API_TOKEN,
    CI_API_V4_URL,
    CI_PROJECT_ID,
    MILESTONE
} = process.env;

// ---------------------------------------------------------------------------------------------------------------------

axios.defaults.headers.common['PRIVATE-TOKEN'] = API_TOKEN;

// ---------------------------------------------------------------------------------------------------------------------

// If milestone's already set, we do nothing
if(MILESTONE) { process.exit(0); }

// Get the current open milestones
const { data } = await axios.get(`${ CI_API_V4_URL }/projects/${ CI_PROJECT_ID }/milestones?state=active`);
if(data.length > 0)
{
    // Sort the milestones by their creation time. (lower iid means created first)
    const milestones = data.concat()
        .sort((milestoneA, milestoneB) =>
        {
            return semver.compare(semver.coerce(milestoneA.title), semver.coerce(milestoneB.title));
        });

    process.env.MILESTONE = milestones[0].title;
    process.env.MILESTONE_ID = milestones[0].id;

    // Must output a bash script to execute
    console.log(`export MILESTONE=${ milestones[0].title }; export MILESTONE_ID=${ milestones[0].id }`);
}
else
{
    // Must output a bash script to execute
    console.log('echo "No current milestones found. Skipping."');
}

// ---------------------------------------------------------------------------------------------------------------------

