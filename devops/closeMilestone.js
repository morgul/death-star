#!/usr/bin/env node

// ---------------------------------------------------------------------------------------------------------------------
// Set the current Milestone
//
// This script should be called on it's own, and not executed through bash.
// ---------------------------------------------------------------------------------------------------------------------

import axios from 'axios';

// ---------------------------------------------------------------------------------------------------------------------

const {
    API_TOKEN,
    CI_API_V4_URL,
    CI_PROJECT_ID,
    MILESTONE_ID
} = process.env;

// ---------------------------------------------------------------------------------------------------------------------

axios.defaults.headers.common['PRIVATE-TOKEN'] = API_TOKEN;

// ---------------------------------------------------------------------------------------------------------------------

// If we have MILESTONE_ID set, we delete it, otherwise we skip.
if(MILESTONE_ID)
{
    await axios.put(`${ CI_API_V4_URL }/projects/${ CI_PROJECT_ID }/milestones/${ MILESTONE_ID }`, {
        id: CI_PROJECT_ID,
        milestone_id: MILESTONE_ID,
        state_event: 'close'
    });
}
else
{
    console.log('Milestone not set. Skipping.');
}

// ---------------------------------------------------------------------------------------------------------------------

