#!/usr/bin/env node

// ---------------------------------------------------------------------------------------------------------------------
// Create the next milestone, based off the current milestone.
//
// This script should be called on it's own, and not executed through bash.
// ---------------------------------------------------------------------------------------------------------------------

import axios from 'axios';
import semver from 'semver';

// ---------------------------------------------------------------------------------------------------------------------

const {
    API_TOKEN,
    CI_API_V4_URL,
    CI_PROJECT_ID
} = process.env;

// ---------------------------------------------------------------------------------------------------------------------

axios.defaults.headers.common['PRIVATE-TOKEN'] = API_TOKEN;

// ---------------------------------------------------------------------------------------------------------------------

// Get the current open milestones
const { data } = await axios.get(`${ CI_API_V4_URL }/projects/${ CI_PROJECT_ID }/milestones?state=active`);
if(data.length > 0)
{
    // Sort the milestones by their creation time. (lower iid means created first)
    const milestones = data.concat()
        .sort((milestoneA, milestoneB) =>
        {
            return semver.compare(semver.coerce(milestoneA.title), semver.coerce(milestoneB.title));
        });

    // Calculate the next milestone version
    const nextSemver = semver.inc(semver.coerce(milestones[0].title), 'minor');
    const nextMilestoneVer = `${ semver.major(nextSemver) }.${ semver.minor(nextSemver) }`;

    // We check to see if, somehow, the milestone already exists. If not, we create it.
    const existingMilestone = milestones.find((milestone) => milestone.title === nextMilestoneVer);
    if(!existingMilestone)
    {
        await axios.post(`${ CI_API_V4_URL }/projects/${ CI_PROJECT_ID }/milestones`, {
            id: CI_PROJECT_ID,
            title: nextMilestoneVer
        });
    }
}
else
{
    console.log('No current milestones found. Skipping.');
}

// ---------------------------------------------------------------------------------------------------------------------

